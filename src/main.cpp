/*
licence  : GPL v.3 et CC BY 3.0 FR, plus dinfo sur : https://www.gnu.org/licenses/quick-guide-gplv3 et https://creativecommons.org/licenses/by/3.0/fr/
Réalisateur du projet   : Ethan BEAUVAIS-GUIBERT et Adrien
notre site web : https://ospcar.github.io/
Nous contacter : https://ospcar.github.io/contact.html
Tous les codes et schémas : https://ospcar.github.io/code.html
Suivre le projet : https://ospcar.github.io/suividirect.html

Comment ouvrire les fichiers Arduino (atom): https://projetsdiy.fr/bien-demarrer-platformio-ide-arduino-esp8266-esp32-stm32/
*/

//programme de la voiture

#include <Arduino.h>
#include "RF24.h"
#include "printf.h"
#include "nRF24L01.h"
#include "RF24_config.h"
#include <Servo.h>
#include <TinyGPS++.h>
//#include <SoftwareSerial.h>

// The TinyGPS++ object
TinyGPSPlus gps;

// The serial connection to the GPS device
//SoftwareSerial ss(4, 3); //Rx - Tx

void direction ();
void vitesse ();
void wifi_traitement(); //fonction qui envoie les informations (valeur; address) à la voiture

RF24 radio_voiture(9,10); //CE,CSN  Pin du module radio (NRF24L01 +)

const byte address_envoie [6] = "00010"; //Adress de la voiture pour l'Envoie
const byte address_reception [6] = "00001";

Servo servo; //déclaration du servo-moteur


struct data_Package // Max 32 bytes NRF24L01 buffer limit
{
  int direction;
  int vitesse;
  bool ping = false;
  bool pong = true;
  byte gps_speed;//Speed by the gps module
  byte battery_level_car = 0;
};
data_Package data;


void setup ()
{
  printf_begin();
  Serial.begin (9600);
  servo.attach(2);
  radio_voiture.begin();
  radio_voiture.openWritingPipe(address_envoie);
  radio_voiture.openReadingPipe(0, address_reception);
  radio_voiture.setPALevel(RF24_PA_MIN);
  radio_voiture.startListening();

  // Pins Motor (forward gear)
  pinMode (5, OUTPUT); // PWM MOSFET N
  pinMode (7, OUTPUT);// MOSFET P
  digitalWrite(5, LOW); // Turn off the motor
  digitalWrite(7, HIGH);

  //Pins Motor (reverse gear)
  pinMode (6, OUTPUT); // PWM MOSFET N
  pinMode (8, OUTPUT);// MOSFET P
  digitalWrite(6, LOW); // Turn off the motor
  digitalWrite(8, HIGH);
}
int vit = 0;
unsigned long timeout = millis();

void loop ()
{
  if (radio_voiture.available())
  {
    while (radio_voiture.available())
    radio_voiture.read(&data, sizeof (data_Package));
    timeout = millis();
  }
  else if (millis() - timeout > 1000)//If the car is not connected to the RC then the motor will be stoped
  {
    data.vitesse = 0;
    Serial.println (timeout);
  }

  servo.write (data.direction);

  if (data.vitesse > 0)//(forward gear)
  {
    digitalWrite (6,LOW);
    analogWrite (5, data.vitesse);
  }
  else if (data.vitesse < 0) // Reverse gear
  {
    int vitesse = abs(data.vitesse);//Calculates the absolute value of a number.
    digitalWrite (5, LOW);
    analogWrite (6, vitesse);
  }
  else
  {//Stop the motor
    digitalWrite (5, LOW);
    digitalWrite (6, LOW);
  }

  while (Serial.available() > 0)
  if (gps.encode(Serial.read()))
  data.gps_speed = gps.speed.kmph();

  data.gps_speed = gps.speed.kmph();
  if (analogRead(A0) > 950)
  data.battery_level_car = map (analogRead(A0),950,1023,0,5); // battery_level_car
  else
  data.battery_level_car = 0;
  if (data.ping == true)
  {
    data.pong = true;
    radio_voiture.stopListening();
    for (int i = 0; i < 5; i++)
    {
      radio_voiture.write (&data, sizeof (data_Package));
      delay (5);
    }
    radio_voiture.startListening();
    data.ping = false;
  }
}
